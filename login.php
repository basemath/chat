<?php
  require_once("inc/functions.php");
  session_start();

  $error_message = "";
  if( logged_in() && isset($_SESSION["username"]) ){
    header("location: ".HOME_URL);
  }

  //if the user has submitted the login form, process it
  if( isset($_POST["password"]) ){
    attempt_login($_POST["password"]);
  }
  else if( isset($_POST["urmomsface"]) ){
    if( register_username($_POST["urmomsface"]) ){
      header("location: ".HOME_URL);
    }
    else{
      $error_message = "Uh-oh. That username is already in use. Please select a new one.";
    }
  }
?>
<!doctype html>
<html>
<head>
  <title>Login</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php

if( logged_in() ){

  if( $error_message != "" ){
    echo "<div class='alert-error'>$error_message</div>";
  }
?>
  <h1>Who are you?</h1>
  <form action="<?php echo LOGIN_URL; ?>" method="post">
    <label for="username">Username:</label>
    <input type="text" id="urmomsface" name="urmomsface" maxlength="255" value="<?php echo isset($_SESSION["username"]) ? $_SESSION["username"] : ""; ?>" />
    <br />
    <button type="submit" value="submit" name="submit">Submit</button>
  </form>
<?php
}
else{
?>
  <form action="<?php echo LOGIN_URL; ?>" method="post">
    <label for="password">Password:</label>
    <input type="password" name="password" id="password" maxlength="255" />
    <br />
    <button type="submit" value="login" name="login">Log In</button>
  </form>
<?php
}
?>
</body>
</html>
