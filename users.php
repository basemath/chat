<?php

if( isset($_GET["u"]) ){

  require_once "inc/functions.php";
  session_start();

  if( logged_in() ){

    header("Content-type: application/json");
    echo get_logged_in_users_json();

  }

}

?>
