<?php
  require_once("inc/functions.php");
  session_start();
  validate_logged_in();

  handle_parameters();
?>
<!doctype html>
<html>
<head>
  <title>Chat</title>
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <script src="js/jquery.js"></script>
  <script src="js/chat.js.php"></script>
  <script src="js/ui.js"></script>
</head>
<body>
<div class="sidebar">
  <a class="logout-button" href="logout.php">&larr; logout</a>
  <span class="divider">View:</span>
  <a class="view-button active" href="#chat-view">chat</a>
  <a class="view-button" href="#bulletin-board">bulletin board</a>
  <span class="divider">Users Online:</span>
  <div id="users">
  </div>
</div>
<div class="container" id="chat-view">
  <h1>Hello, <?php echo $_SESSION["username"]; ?></h1>
  <div id="chat-log"></div>
  <div id="controls">
    <textarea id="input" rows="8" cols="40"></textarea>
    <br />
    <button id="send" value="send" name="send">send</button>
  </div>
</div>
<div class="container hide" id="bulletin-board">
  <h1>Bulletin Board</h1>
  <div class="post-new-message">
    <form action="<?php echo HOME_URL; ?>" method="post">
      <textarea id="post-input" rows="8" cols="40" name="post-message"></textarea>
      <br />
      <button id="post" value="post" name="post">Pin this message on the bulletin board, please. Thank you.</button>
    </form>
  </div>
  <div class="messages">
    <?php display_bulletin_messages(); ?>
  </div>
</div>
</body>
</html>
