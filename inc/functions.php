<?php

/*
 * setting default timezone to supress warnings
 */
date_default_timezone_set('America/Chicago');

/*
 * define production and development vars for seamless integration
 * locally and live
 */
define("DEVELOPMENT", 1);
define("PRODUCTION", 2);
define("JSON_FORMAT", 3);
define("ARRAY_FORMAT", 4);

if( $_SERVER["SERVER_NAME"] == "dev" ){
  define("ENV", DEVELOPMENT);
}
else{
  define("ENV", PRODUCTION);
}

/*
 * global constants
 */
define("HOME_URL", ENV == DEVELOPMENT ? "/~david/chat/" : "/chat/");
define("LOGIN_URL", HOME_URL . "login.php");
define("FEED_URL", HOME_URL . "feed.php");

/*
 * this file may be included from files that are in various directories.
 * the following code is some dynamic jank I use to define a correct
 * relative path to the base chat folder
 */
$depth = substr_count(getcwd(), DIRECTORY_SEPARATOR);
$depth_of_base_dir = ENV == DEVELOPMENT ? 4 : 3;
if( $depth < $depth_of_base_dir ){
  die("depth < depth_of_base_dir :(");
}
$path = "";
for( $i=0; $i<$depth - $depth_of_base_dir; $i++ ){
  $path .= ".." . DIRECTORY_SEPARATOR;
}
define("BASE_DIR", $path);
define("SESSION_SAVE_PATH", BASE_DIR . "sessions");
define("TEXTFILES_DIR", BASE_DIR . "textfiles");
define("CHAT_LOG", TEXTFILES_DIR . DIRECTORY_SEPARATOR . "chat.log");
define("BULLETIN_LOG", TEXTFILES_DIR . DIRECTORY_SEPARATOR . "bulletin.log");

define("MAIN_PASSWORD", "hehe this is very sneaky");

/*
 * set session expiration time to 60 minutes
 */
session_cache_expire(60);              //measured in minutes
define("INACTIVITY_TIMEOUT", 60 * 60); //measured in seconds

/*
 * set directory sessions will be saved in
 */
if( !file_exists(SESSION_SAVE_PATH) ){
  mkdir(SESSION_SAVE_PATH, 0777);
}
session_save_path(SESSION_SAVE_PATH);

/*
 * ensure only cookies are used for security
 */
ini_set('session.use_only_cookies', '1');

/*
 * ==============================================================================================================
 * global functions
 * ==============================================================================================================
 */
function fingerprint(){
  return md5($_SERVER["HTTP_USER_AGENT"] + "daniel is ironing his clothes");
}

function redirect(){
  header("location: " . LOGIN_URL);
}

function log_in(){
  $_SESSION["uid"] = md5("".mt_rand(0, 100).mt_rand(0, 100).mt_rand(0, 100).mt_rand(0, 100).mt_rand(0, 100).$_SERVER["HTTP_USER_AGENT"]);
  $_SESSION["fingerprint"] = fingerprint();
  $_SESSION["chat_start"] = filesize(CHAT_LOG);

  //clear chat log if possible.
  //it may not have gotten wiped if users didn't log out correctly
  clear_chat_log();
}

function attempt_login($password){
  $pass = md5($password);
  if( $pass == md5(MAIN_PASSWORD) ){
    log_in();
  }
}

function log_out(){
  // Unset all of the session variables.
  $_SESSION = array();

  //must destroy the session cookie, too!
  $params = session_get_cookie_params();
  setcookie(session_name(), '', time() - 42000,
      $params["path"], $params["domain"],
      $params["secure"], $params["httponly"]
  );

  //finally, destroy the session, send to login page
  session_destroy();

  //clear chat log if possible
  clear_chat_log();

  //send back to login page
  redirect();
}

function logged_in(){
  return isset($_SESSION["uid"]) && isset($_SESSION["fingerprint"]) && $_SESSION["fingerprint"] == fingerprint();
}

/*
 * Note: this does not do the same thing as logged_in()
 * This checks all the credentials needed for a user to start chatting
 *
 * logged_in will return true even if they have not registered a username yet
 */
function validate_logged_in(){
  if( !logged_in() || !isset($_SESSION["username"]) ){
    redirect();
    die();
  }
}

function sanitize_name($input){
  return substr(trim(preg_replace('/\W/', '', strip_tags($input))), 0, 255);
}

function register_username($username){
  $username = sanitize_name($username);
  if( !in_array($username, get_logged_in_users(ARRAY_FORMAT)) && $username != "" ){
    $_SESSION["username"] = $username;
    return true;
  }
  else{
    return false;
  }
}

function sanitize_chat_input($input){
  /*
   * note: it's important to use ENT_QUOTES and convert the quotes,
   * since quotes are being used as signals for the json format
   * that the data is being stored in. if the quotes weren't escaped
   * or converted into html entities, stuff would get messed up
   */
  return preg_replace("/\n/", "\\n", preg_replace("{\\\}", "&#92;", htmlentities(trim($input), ENT_QUOTES)));
}

function write_chat_message($message){
  $message = sanitize_chat_input($message);
  if( $message != "" ){
    $contents = file_get_contents(CHAT_LOG);
    /*
     * all data is being saved in json format, so the client side js can
     * quickly interpret and use it
     */
    if( $contents != "" ){
      $contents .= ",\n";
    }
    $contents .= '{"a":"' . $_SESSION["username"] . '","m":"' . $message . '"}';
    file_put_contents(CHAT_LOG, $contents);
  }
}

function get_chat_after($offset){
  $return_text = "";

  /*
   * note: these file checks are here not so much
   * to prevent errors, but just so that httpd will be
   * the author of the files, and thus be ensured
   * write permissions
   */
  if( !file_exists(TEXTFILES_DIR) ){
    mkdir(TEXTFILES_DIR, 0777);
  }
  if( !file_exists(CHAT_LOG) ){
    $fh = fopen(CHAT_LOG, 'w');
    fclose($fh);
  }

  /*
   * we must add 2 to offset if it is greater than 0, because each
   * entry is delimited by ",\n"
   * If we don't add 2 to the offset, ",\n" will be prepended to the
   * data.
   */
  if( $offset > 0 ){
    $offset += 2;
  }
  //get the contents of the log after the offset
  $return_text = file_get_contents(CHAT_LOG, null, null, $offset);
  if( !$return_text ){
    $return_text = "";
  }

  return $return_text;
}

function get_logged_in_users($format_type){
  $sessions = scandir(SESSION_SAVE_PATH);
  if( $format_type == JSON_FORMAT ){
    $usernames = '{"users":[';
  }
  else{
    $usernames = array();
  }
  $sess = "";
  foreach( $sessions as $session_file ){
    $sess = file_get_contents(SESSION_SAVE_PATH . DIRECTORY_SEPARATOR . $session_file);
    if( strlen($sess) > 0 ){
      preg_match("/username\|s:\d+:\"(\w+)\"/", $sess, $matches);
      if( isset($matches[1]) ){
        if( $format_type == JSON_FORMAT ){
          $usernames .= '"'.$matches[1].'",';
        }
        else{
          array_push($usernames, $matches[1]);
        }
      }
    }
  }
  if( $format_type == JSON_FORMAT ){
    //chop off trailing comma
    $usernames = substr($usernames, 0 , -1);
    //finish formatting
    $usernames .= ']}';
  }
  return $usernames;
}

function clear_chat_log(){
  if( sizeof(get_logged_in_users(ARRAY_FORMAT)) == 0 ){
    shred_file(CHAT_LOG);
    return true;
  }
  else{
    return false;
  }
}

function shred_file($filename){
  fill_with_random_data($filename, filesize(CHAT_LOG));
  file_put_contents($filename, "");
}

function fill_with_random_data($file_name, $size_in_bytes)
{
   $data = str_repeat(rand(0,9), $size_in_bytes);
   file_put_contents($file_name, $data);
}

function pin_bulletin_message($author, $message){
  $message = sanitize_chat_input($message);
  if( $message != "" ){
    $time = time();
    $write_this = file_get_contents(BULLETIN_LOG);

    //NOTE: > is a safe delimiter, since it gets turned into &gt; by sanitize_chat_input
    $write_this .= "$time>$author>$message\n";
    file_put_contents(BULLETIN_LOG, $write_this);
  }
}

function display_bulletin_messages(){
  if( file_exists(BULLETIN_LOG) ){
    $lines = file(BULLETIN_LOG);
    for( $i=count($lines); $i >= 0; $i--){
      $line = $lines[$i];
      if( $line ){
        $parts = explode(">", $line);
        echo "<div class=\"bulletin-meta\">Posted by \n";
        echo "<span class=\"bulletin-author\">".$parts[1]."</span>\n";
        echo "<span class=\"bulletin-date\">".ago($parts[0])."</span>\n";
        echo "<a class=\"delete-button\" href=\"?delete=".$parts[0]."\">[&times;]</a>\n";
        echo "</div>\n";
        echo "<div class=\"bulletin-message\">".preg_replace("/\\\\n/", "<br />", $parts[2])."</div>\n";
      }
    }
  }
  else{
    fopen(BULLETIN_LOG, "w");
  }
}

function delete_bulletin_message($timestamp){
  //re-write the file with everything but the message to delete
  if( file_exists(BULLETIN_LOG) ){
    $lines = file(BULLETIN_LOG);
    foreach ($lines as $line_num => $line) {
      $parts = explode(">", $line);
      if( $parts[0] != "".$timestamp ){
        $write_this .= $line;
      }
    }
    file_put_contents(BULLETIN_LOG, $write_this);
  }
}

function handle_parameters(){
  if( isset($_POST["post-message"]) ){
    pin_bulletin_message($_SESSION["username"], $_POST["post-message"]);
    //prevents re-submission by page refresh
    header("location: " . HOME_URL . "#bulletin-board");
  }
  else if( isset($_GET["delete"]) ){
    $delete_id = trim(addslashes($_GET["delete"]));
    if( $delete_id != "" && is_numeric($delete_id)){
      delete_bulletin_message($delete_id);
      //prevents re-submission by page refresh
      header("location: " . HOME_URL . "#bulletin-board");
    }
  }
}


function ago($time){
   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = "ago";

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   if($difference != 1) {
       $periods[$j].= "s";
   }

   return "$difference ".$periods[$j]." ago";
}

?>
