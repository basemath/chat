<?php

require_once("inc/functions.php");
session_start();

/*
 * only process if the request is coming from our javascript
 */
if( true ) {


  //make sure user is logged in
  if( logged_in() && isset($_SESSION["username"]) ){

    if( isset($_POST["message"]) ){
      write_chat_message($_POST["message"]);
    }
    else if( isset($_GET["a"]) ){

      //make sure parameter is numeric
      if( is_numeric($_GET["a"]) ){

        //get the intval of the parameter
        $offset = intval($_GET["a"]) + $_SESSION["chat_start"];
        $file_size = filesize(CHAT_LOG);

        /*
         * if the offset requested is greater than the file size,
         * it means that some of the file has been deleted after the
         * client begain it's session. This check will gracefully
         * accomodate
         */
        if( $offset > $file_size ){
          $_SESSION["chat_start"] = $file_size;
          header("Content-type: application/json");
          echo '{"pointer":0}';
        }
        //only read from file if there's reading to do
        else if( $offset < $file_size ){
          header("Content-type: application/json");
          $messages = get_chat_after($offset);
          echo '{"pointer":' . ($file_size - $_SESSION["chat_start"]) . ',"messages":['.$messages.']}';
        }
        else{
          echo "";
        }
      }
      else{
        echo "";
      }
    }
    else if( isset($_GET["u"]) ){
      header("Content-type: application/json");
      echo get_logged_in_users(JSON_FORMAT);
    }
  }
  else{
    header("Content-type: application/json");
    echo '{"error":"Your session has timed out.\nPlease log back in to continue chatting."}';
  }
}
else{
  http_response_code(404);
}

?>
