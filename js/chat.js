$(function(){
  "use strict"

  //global vars
  var chat_url = "<?php echo FEED_URL; ?>"
  var $input = $("#input")
  var chat_pointer = 0
  var $chat_window = $("#chat-log")
  var update_frequency = 1000
  var new_html = ""
  var last_author = ""
  var colors = ["f03737", "4a37f0", "37a9f0", "37f0ce", "47f037", "03aDa5", "e6d847", "e68647", "9103ad"]
  var authors = []
  var author_colors = {}


  function shuffle(o){
      for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
      return o;
  }

  function author_exists(author){
    for( var i=0; i<authors.length; i++ ){
      if( authors[i].author == author ){
        return true;
      }
    }
    return false;
  }

  function color_taken(color){
    for( var i=0; i<authors.length; i++ ){
      if( authors[i].color == color ){
        return true;
      }
    }
    return false;
  }

  function give_author_color(author){
    var color = ""
    if( authors.length < colors.length ){
      colors = shuffle(colors)
      for( var i=0; i<colors.length; i++ ){
        if( !color_taken(colors[i]) ){
          color = colors[i]
        }
      }
    }
    else{
      color = shuffle(colors)[0]
    }
    author_colors[author] = color
    authors.push({author:author,color:color})
  }

  //function to update the chat window
  function update(){
    $.get(chat_url, {
        a: chat_pointer
      }, function(data){
      //there will only be data returned if
      //there is new stuff in the chat log

      if( data && data.pointer != undefined ){
        //cache the new chat pointer
        chat_pointer = data.pointer
      }
      if( data && data.messages && data.messages.length ){

        new_html = ""
        for( var i=0; i<data.messages.length; i++ ){
          var author = data.messages[i].author
          //if( !author_exists(author) ){
          //  give_author_color(author)
          //}
          if( author != last_author ){
            new_html += '<div class="message new-author">'
            new_html += '<div class="author">'+author+':</div>'
            last_author = author
          }
          else{
            new_html += '<div class="message">'
          }
          new_html += data.messages[i].message.replace(/\n/g, "<br />")+'</div>'
        }

        //using append will keep highlighted text highlighted
        //inside the chat window while updating it
        $chat_window.append(new_html).show()

        //now make sure the chat window is scrolled down
        $chat_window.animate({ scrollTop: $chat_window[0].scrollHeight}, 1000);
      }
    })
  }

  //keep the chat window updated
  var updateInterval = setInterval(update, update_frequency);

  //set focus on textarea when page loads
  $input.focus()

  //function to submit a message
  function send_message(message){
    if( message ){
      $.post(chat_url, {
        message: message
      })
      $input.val("").focus()
      var old_border = $input.css("border")
      $input.css("border", "1px solid #00ff00");
      setTimeout(function(){
        $input.css("border", old_border)
      }, 500)
    }
  }

  //ui mappings
  $("#send").click(function(){
    send_message($input.val())
  })

  $input.keyup(function (event) {
    if (event.keyCode == 13 && event.shiftKey) {
      event.stopPropagation()
      return false
    }
    else if(event.keyCode == 13){
      send_message($input.val())
      return false
    }
  })
})
