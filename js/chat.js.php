<?php
  require_once "../inc/functions.php";
  header("Content-type: application/javascript");
?>
$(function(){
  "use strict"

  //global vars
  var chat_url = "<?php echo FEED_URL; ?>"
  var $input = $("#input")
  var chat_pointer = 0
  var $chat_window = $("#chat-log")
  var update_chat_frequency = 1000 //one second
  var update_users_frequency = 10000 //ten seconds
  var new_html = ""
  var last_author = ""
  var update_in_progress = false
  var title_alert_interval = null
  var title_default = String(document.title)
  var window_focus = true;

  //function to submit a message
  function send_message(message){
    if( message ){
      $.post(chat_url, {
        message: message
      })
      $input.val("").focus()
      var old_border = $input.css("border")
      $input.css("border", "4px solid #4040ff");
      setTimeout(function(){
        $input.css("border", old_border)
      }, 400)
    }
  }

  //function to update the chat window
  function update_chat(){
    if( !update_in_progress ){
      update_in_progress = true
      $.get(chat_url, {
          a: chat_pointer
        }, function(data){
        //there will only be data returned if
        //there is new stuff in the chat log

        if( data && data.pointer != undefined ){
          //cache the new chat pointer
          chat_pointer = data.pointer
        }
        if( data && data.messages && data.messages.length ){

          display_new_message(data)
        }
        else if( data && data.error ){
          alert(data.error)
          clearInterval(chatInterval)
        }
        update_in_progress = false
      })
    }
  }

  function display_new_message(data){
    new_html = ""
    for( var i=0; i<data.messages.length; i++ ){
      var author = data.messages[i].a
      //if( !author_exists(author) ){
      //  give_author_color(author)
      //}
      if( author != last_author ){
        new_html += '<div class="message new-author">'
        new_html += '<div class="author">'+author+':</div>'
        last_author = author
      }
      else{
        new_html += '<div class="message">'
      }
      new_html += data.messages[i].m.replace(/\n/g, "<br />")+'</div>'
    }

    //using append will keep highlighted text highlighted
    //inside the chat window while updating it
    $chat_window.append(new_html).show()

    //do flashy things
    flash_display()
    if( !window_focus ) title_alert("new message from " + author)

    //now make sure the chat window is scrolled down
    $chat_window.animate({ scrollTop: $chat_window[0].scrollHeight}, 1000);
  }

  function flash_display(){
    var old_border = $chat_window.css("border-left")
    $chat_window.css("border-left", "4px solid #4040ff");
    $chat_window.css("border-right", "4px solid #4040ff");
    setTimeout(function(){
      $chat_window.css("border-left", old_border)
      $chat_window.css("border-right", old_border)
    }, 400)
  }

  function update_users(){
    $.get(chat_url, {
      u: "all"
    }, function(data){
      if( data && data.users ){
        var $users_div = $("#users")
        $users_div.html('')
        for( var i=0; i<data.users.length; i++ ){
          $users_div.append('<span class="user">'+data.users[i]+'</span>').show()
        }
      }
    })
  }

  function title_alert(message){
    var flip = 1
    title_alert_interval = setInterval(function(){
      flip = flip*-1
      document.title = flip < 0 ? message : title_default
    }, 1000)
  }

  function end_title_alert(){
    clearInterval(title_alert_interval)
    document.title = title_default
  }

  //keep the chat window updated
  var chatInterval = setInterval(update_chat, update_chat_frequency);

  //keep the online users list updated
  var usersInterval = setInterval(update_users, update_users_frequency);

  //do updates right away
  update_chat()
  update_users()

  //set focus on textarea when page loads
  $input.focus()

  //ui mappings
  $("#send").click(function(){
    send_message($input.val())
  })

  $(window).focus(function() {
    window_focus = true
    end_title_alert()
  })
  .blur(function() {
    window_focus = false
  })

  $input.keyup(function (event) {
    if (event.keyCode == 13 && event.shiftKey) {
      event.stopPropagation()
      return false
    }
    else if(event.keyCode == 13){
      send_message($input.val())
      return false
    }
  })
})
