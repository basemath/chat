$(function(){
  var bookmark = /(#[^#]+)$/;
  var match = bookmark.exec(window.location);
  if( match ){
    var id = match[0];
    if( id ){
      $('.container').hide()
      $(id).show()
      $('.view-button').removeClass('active')
      $("a[href='"+id+"']").addClass('active')
    }
  }

  $('.view-button').click(function(){
    $('.view-button').removeClass('active')
    $(this).addClass('active')
    $('.container').hide()
    $($(this).attr('href')).show()
  })
  $('.delete-button').click(function(){
    return confirm('Are you sure you want to delete this post?')
  })
})
